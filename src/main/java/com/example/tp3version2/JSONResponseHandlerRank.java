package com.example.tp3version2;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;



public class JSONResponseHandlerRank {

    private static final String TAG = JSONResponseHandlerRank.class.getSimpleName();

    private Team team;


    public JSONResponseHandlerRank(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readRank(reader);
        } finally {
            reader.close();
        }
    }

    public void readRank(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("table")) {
                readArrayRank(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayRank(JsonReader reader) throws IOException {

        String nomEquipe = "";
        boolean trouveId = false;
        reader.beginArray();
        int nb = 1;
        while (reader.hasNext() ) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("name")) {
                    nomEquipe = reader.nextString();
                    if (nomEquipe.equals(team.getName())){
                        trouveId = true;
                        team.setRanking(nb);
                    }
                }else if (name.equals("total") && trouveId){
                    team.setTotalPoints(reader.nextInt());
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();

    }
}
