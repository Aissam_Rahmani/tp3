package com.example.tp3version2;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JSONResponseHandlerMatch {

    private static final String TAG = JSONResponseHandlerMatch.class.getSimpleName();

    private Team team;


    public JSONResponseHandlerMatch(Team team) {
        this.team = team;


    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readMatch(reader);
        } finally {
            reader.close();
        }
    }

    public void readMatch(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("results")) {
                readArrayMatchs(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayMatchs(JsonReader reader) throws IOException {
        Match match = new Match();
        reader.beginArray();
        int nb = 0; // only consider the first element of the array
        while (reader.hasNext() ) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (nb==0) {
                    if (name.equals("idEvent")) {
                        match.setId(reader.nextLong());
                    } else if (name.equals("strEvent")) {
                        match.setLabel(reader.nextString());
                    } else if (name.equals("strHomeTeam")) {
                        match.setHomeTeam(reader.nextString());
                    } else if (name.equals("strAwayTeam")) {
                        match.setAwayTeam(reader.nextString());
                    } else if (name.equals("intHomeScore")) {
                        match.setHomeScore(reader.nextInt());
                    } else if (name.equals("intAwayScore")) {
                        match.setAwayScore(reader.nextInt());
                    } else {
                        reader.skipValue();
                    }
                }  else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
        team.setLastEvent(match);
    }

}
