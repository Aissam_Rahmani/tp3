package com.example.tp3version2;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    RecyclerView recyclerView;
    public static SwipeRefreshLayout mySwipe;


    public static MyAdapter adapter;

    public static SportDbHelper dbHelper;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        MainActivity.dbHelper = new SportDbHelper(this);

        FloatingActionButton fab = findViewById(R.id.fab);



        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, NewTeamActivity.class);
                MainActivity.this.startActivityForResult(myIntent,1);
            }
        });


         //  dbHelper.populate();

        this.recyclerView = findViewById(R.id.recylView);
        this.mySwipe = findViewById(R.id.myswipe);

        // Initialiser adapter:

        adapter = new MyAdapter(this);

        this.recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mySwipe.setOnRefreshListener(this);

        // Remove with slide
        new ItemTouchHelper(deletItemInListe).attachToRecyclerView(recyclerView);

    }


    // Test network success
    public static boolean testIfIsConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo netWorkInfo = connectivityManager.getActiveNetworkInfo();
        return netWorkInfo != null && netWorkInfo.isConnectedOrConnecting();
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (data !=null && resultCode == RESULT_OK) {

            Team team = (Team) data.getParcelableExtra(Team.TAG);

            if (team != null) {
                MainActivity.dbHelper
                        .addTeam(team);
                Toast.makeText(this, "L'équipe "+team.getName()+" ajouté avec succé", Toast.LENGTH_SHORT).show();
                adapter.notifyDataSetChanged();
            }

        } else {
            Toast.makeText(this, "Error de l'ajout!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {

        List<Team> allTeams = dbHelper.getAllTeams();



        for (Team team : allTeams) {

            UpdateAllTask runningTask = new UpdateAllTask();
            runningTask.execute(team);


        }

    }



    ItemTouchHelper.SimpleCallback deletItemInListe = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            int position = viewHolder.getAdapterPosition();
            long id = MainActivity.dbHelper.getAllTeams()
                    .get(position).getId();

            MainActivity.dbHelper.deleteTeam(id);

            adapter.notifyDataSetChanged();
        }
    };

    private final class UpdateAllTask extends AsyncTask<Object, Void, List<Team>> {
        private List<Team> allTeams;

        @Override
        protected void onPreExecute() {
            allTeams = MainActivity.dbHelper.getAllTeams();

            adapter.notifyDataSetChanged();
            recyclerView.setAdapter(adapter);

            super.onPreExecute();
        }


        @Override
        protected List<Team> doInBackground(Object... objects) {

            List<Team> allTeams = MainActivity.dbHelper.getAllTeams();

            for (Team team : allTeams) {
                boolean run = true;
                while (run) {
                    URL url = null;
                    HttpURLConnection urlConnection = null;
                    try {
                        // Team
                        // prepare url
                        url = WebServiceUrl.buildSearchTeam(team.getName());
                        urlConnection = (HttpsURLConnection) url.openConnection();
                        JSONResponseHandlerTeam jsonResponseHandlerTeam = new JSONResponseHandlerTeam(team);
                        try {
                            // read data
                            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                            jsonResponseHandlerTeam.readJsonStream(in);
                        } finally {
                            urlConnection.disconnect();
                        }
                        // Rank
                        // prepare url
                        url = WebServiceUrl.buildGetRanking(team.getIdLeague());
                        urlConnection = (HttpsURLConnection) url.openConnection();
                        try {
                            // read data
                            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                            JSONResponseHandlerRank jsonResponseHandlerRank = new JSONResponseHandlerRank(team);
                            jsonResponseHandlerRank.readJsonStream(in);
                        } finally {
                            urlConnection.disconnect();
                        }

                        // Laste Evenet
                        // prepare url
                        url = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                        urlConnection = (HttpsURLConnection) url.openConnection();
                        try {
                            // read data
                            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                            JSONResponseHandlerMatch jsrEvent = new JSONResponseHandlerMatch(team);
                            jsrEvent.readJsonStream(in);
                        } finally {
                            urlConnection.disconnect();
                            run = false;
                        }

                        dbHelper.updateTeam(team);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return allTeams;
        }



        @Override
            protected void onPostExecute (List<Team> allTeams){
                super.onPostExecute(allTeams);
                adapter.notifyDataSetChanged();
              recyclerView.setAdapter(adapter);
                mySwipe.setRefreshing(true);

            }

        }


    // Class Adapter:

    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

        private Context context;

        public MyAdapter(Context context) {
            this.context = context;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater myInflater = LayoutInflater.from(context);
            View view = myInflater.inflate(R.layout.team_item,parent,false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

            Team team = MainActivity.dbHelper
                    .getAllTeams()
                    .get(position);

            new LoadImageTask(holder.imageIcone, team).execute();
            holder.teamName.setText(team.getName());
            holder.leagueName.setText(team.getLeague());
            holder.lastMatch.setText(team.getLastEvent().toString());

            holder.consLay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent myIntent = new Intent(context, TeamActivity.class);
                    myIntent.putExtra(Team.TAG, dbHelper.getAllTeams().get(position));
                    context.startActivity(myIntent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return dbHelper.getAllTeams().size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            ConstraintLayout consLay;

            ImageView imageIcone;
            TextView teamName;
            TextView leagueName;
            TextView lastMatch;

            public ViewHolder(@NonNull View itemView) {

                super(itemView);

                imageIcone = itemView.findViewById(R.id.imageIcone);
                teamName = itemView.findViewById(R.id.teamName);
                leagueName = itemView.findViewById(R.id.leagueName);
                lastMatch = itemView.findViewById(R.id.lastMatch);
                consLay = itemView.findViewById(R.id.parent_layout);
            }
        }
    }




    // My asynctask IMAGE:


    public static class LoadImageTask extends AsyncTask<String, Void, Bitmap> {

        ImageView imageIcone;
        Team team;

        public LoadImageTask(ImageView imageView, Team team) {
            this.imageIcone = imageView;
            this.team = team;
        }

        protected Bitmap doInBackground(String... urls){
            Bitmap image = null;
            if(team.getTeamBadge() != null){
                try{
                    InputStream in = new java.net.URL(team.getTeamBadge()).openStream();
                    image = BitmapFactory.decodeStream(in);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
            return image;
        }


        @Override
        protected void onPostExecute(Bitmap result) {

            imageIcone.setImageBitmap(result);

        }




    }


}
