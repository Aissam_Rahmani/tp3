package com.example.tp3version2;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
//import com.example.tp3.webservice.WebServiceGet;

public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;

    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    private ImageView imageBadge;
    private Team team;

    private UpdateTeamTask runningTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        team = (Team) getIntent().getParcelableExtra(Team.TAG);

        imageBadge = (ImageView) findViewById(R.id.imageView);

        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.leagueName);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        updateView();

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
            // Verify acces network
                if (MainActivity.testIfIsConnected(TeamActivity.this)) {
                    MainActivity.mySwipe.setRefreshing(true);
                    new UpdateTeamTask().execute();

                }else{
                    MainActivity.mySwipe.setRefreshing(false);
                    new AlertDialog.Builder(TeamActivity.this)
                            .setTitle("Not Connected")
                            .setMessage("You are not connected")
                            .create().show();
                }


            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MainActivity.dbHelper.updateTeam(team);
        MainActivity.adapter.notifyDataSetChanged();
        Intent intent = new Intent(TeamActivity.this, MainActivity.class);
        intent.putExtra(Team.TAG, (Parcelable) team);
        setResult(RESULT_OK,intent);

        finish();
    }

    private void updateView() {

        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());

        if (team.getTeamBadge() != null){ new MainActivity.LoadImageTask(imageBadge, this.team).execute();}
    }


   private final class UpdateTeamTask extends AsyncTask<Object, Integer, Team> {

        @Override
        protected Team doInBackground(Object... params) {
            boolean execute = true;




                URL url = null;
                HttpURLConnection urlConnection = null;
               while(execute==true){
                   try {
                       // Team
                       //Prepar url
                       url = WebServiceUrl.buildSearchTeam(team.getName());
                       urlConnection = (HttpsURLConnection) url.openConnection();
                       JSONResponseHandlerTeam jsonResponseHandlerTeam = new JSONResponseHandlerTeam(team);
                       try {
                           // read data
                           InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                           jsonResponseHandlerTeam.readJsonStream(in);
                       } finally {

                           urlConnection.disconnect();
                       }

                       // Update Rannk
                       //Prepar url
                       url = WebServiceUrl.buildGetRanking(team.getIdLeague());
                       urlConnection = (HttpsURLConnection) url.openConnection();
                       try {
                           // read data
                           InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                           JSONResponseHandlerRank jsonResponseHandlerRank = new JSONResponseHandlerRank(team);
                           jsonResponseHandlerRank.readJsonStream(in);
                       } finally {
                           urlConnection.disconnect();
                       }

                       //Update Laste Event
                       //Prepar url
                       url = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                       urlConnection = (HttpsURLConnection) url.openConnection();
                       try {
                           // read data
                           InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                           JSONResponseHandlerMatch jsonResponseHandlerMatch = new JSONResponseHandlerMatch(team);
                           jsonResponseHandlerMatch.readJsonStream(in);
                       } finally {
                           urlConnection.disconnect();
                       }

                   } catch (IOException e) {
                       e.printStackTrace();
                   }
               }


            return team;

        }


        @Override
        protected void onPostExecute(Team result) {

            updateView();
            MainActivity.dbHelper.updateTeam(result);
            MainActivity.mySwipe.setRefreshing(false);
            super.onPostExecute(result);

        }
    }



    // AlertDialoge
    public static void monALterDialog(Context context){

        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle("Error");
        alertDialog.setMessage("You are not connected");
        alertDialog.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                alertDialog.dismiss();
            }
        }, 10000);
    }

}
